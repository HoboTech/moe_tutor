# Models for Mountain Car that could be used my MBOA (implemented MOE).

import time
import math
import numpy as np

from mountain_car import MountainCar

MOUNTAIN_CAR_MODEL_TYPE_GOOD = 'mountain_car_wilson_good'
MOUNTAIN_CAR_MODEL_TYPE_BAD = 'mountain_car_simple'

class MountainCarLinearModel:
    """Class for representing Mountain Car models"""
    def __init__(self, model_type, dimx, dimy):
        assert(model_type == MOUNTAIN_CAR_MODEL_TYPE_GOOD or
               model_type == MOUNTAIN_CAR_MODEL_TYPE_BAD)
        # TODO(rika): add a bad model. For now just using good model from Wilson.
        self.model_type = model_type
        self.dimx = dimx
        self.dimy = dimy
        self.statsxx = np.zeros([dimx,dimy], dtype=float)
        self.statsxy = np.zeros([dimx,dimy], dtype=float)
        self.statsxr = np.zeros([dimx,1], dtype=float)
        self.trans_beta = np.zeros([dimx,dimy], dtype=float)
        self.rew_beta =np.zeros([dimx,dimy], dtype=float)
        self.npoints = 0

    def update_statistics(self, state, nextstate, reward):
        # Update the statistics for each state variable of the model.
        self.npoints = self.npoints + 1
        step = self.npoints
        self.statsxx = (1/step)*((step-1)*self.statsxx + np.dot(state, state))
        for i in xrange(self.dimy):
            # Only predicting one state variable at a time.
            # Wilson has this, but the dimensions do now match!
            #self.statsxy[:,i] = (1/step)*((step-1)*self.statsxy[:,i] + np.dot(state, nextstate[i]))
            self.statsxy[:,i] = (1/step)*((step-1)*self.statsxy[:,i] + np.dot(state[i], nextstate[i]))
        self.statsxr = 1/(step)*((step-1)*self.statsxr + np.dot(state, reward))

    def compute_params(self):
        for i in xrange(self.dimy):
            self.trans_beta[:,i] = np.linalg.solve(self.statsxx, self.statsxy[:,i])
        self.rew_beta = np.linalg.solve(self.statsxx, self.statsxr)

    @staticmethod
    def get_features(model_type, state, action):
        """Construct linear features from the given state and action."""
        f = np.zeros(5)
        if (model_type == MOUNTAIN_CAR_MODEL_TYPE_GOOD):
            f[0] = state[0]
            c = 1
            # if(nextstate(1) <= -1.5 && action < 0) c=0;
            f[1] = c*state[1]
            f[2] = c*math.cos(3*state[0])
            f[3] = c*action
            f[4] = 1
        elif (model_type == MOUNTAIN_CAR_MODEL_TYPE_BAD):
            f[0] = state[0]
            f[1] = state[1]
            f[2] = math.cos(state[0])
            f[3] = action
            f[4] = 1
        else:
            assert(False)  # unsupported model
        return f

    @staticmethod
    def predict_state(trans_beta, features):
        """Use the estimated transition probability table."""
        return np.dot(features, trans_beta)

    # Wilson has this implementation, but it does not seem to make sense,
    # since it does not return a scalar, but a dimy by 1 vector instead.
    @staticmethod
    def predict_reward(rew_beta, features):
        """Use estimated model to predict reward for one step. """
        return np.dot(features, rew_beta)


def score_model_approx_mountain_car(policy_parameters,
                                    model_type, trans_beta, rew_beta,
                                    num_iter, random_state, debug=False):
    """Evaluates the given learned_model under the true_model. "true" might be
    just the current best (for MBOA, for example)."""
    total_reward = 0
    for itr in xrange(num_iter):
        state = MountainCar.START_STATE
        steps_taken = 0
        while not MountainCar.atGoal(steps_taken, state):
            state_features = MountainCar.getStateFeatures(state)
            action = MountainCar.getAction(policy_parameters, state_features, random_state)
            state_f = MountainCarLinearModel.get_features(model_type, state, action)
            next_state = MountainCarLinearModel.predict_state(trans_beta, state_f)
            steps_taken += 1  # using as reward
            if (debug and (steps_taken < 5 or steps_taken > 395)):
                print "state ", state, " next_state ", next_state, " steps_taken ", steps_taken
        reward = MountainCar.getReward(steps_taken)
        total_reward += reward
    return 1.0*total_reward/num_iter


def adjust_means_mountain_car(points_to_sample, mus, mean_fxn_args):
    """Shift means in mus using the given mean function adjustment arguments"""
    random_state = np.random.RandomState()
    mu_star_adjusted = np.zeros(len(mus))
    assert(len(points_to_sample) == len(mus))
    for idx in xrange(len(mus)):
        trans_beta = np.zeros([len(mean_fxn_args['mboa_fit_trans_beta0']), 2], dtype=float)
        trans_beta[:,0] = mean_fxn_args['mboa_fit_trans_beta0']
        trans_beta[:,1] = mean_fxn_args['mboa_fit_trans_beta1']
        rew_beta = np.zeros([len(mean_fxn_args['mboa_fit_rew_beta0']), 2], dtype=float)
        rew_beta[:,0] = mean_fxn_args['mboa_fit_rew_beta0']
        rew_beta[:,1] = mean_fxn_args['mboa_fit_rew_beta1']
        #print "adjust_means_mountain_car():",\
        #    "trans_beta ", trans_beta, " trans_beta.shape ", trans_beta.shape, \
        #    " rew_beta ", rew_beta, " rew_beta.shape ", rew_beta.shape
        # We used to take just a single sample, and this was very very noisy.
        num_iter = 1  # mountain car domain is not stochastic, so only need 1
        adjustment = mean_fxn_args['beta'] * score_model_approx_mountain_car(
            points_to_sample[idx], mean_fxn_args['mboa_model_type'],
            trans_beta, rew_beta, num_iter, random_state, debug=False)
        mu_star_adjusted[idx] = mus[idx] + adjustment
    return mu_star_adjusted
