# Code to simulate Mountain Car domain.
# Initial Implementation was done by Dexter to try to replicate WilsonJMLR14.
# Further changes done by Rika (added noisy version, deterministic policy, etc).

import math
import numpy

class MountainCar:

    MAX_STEPS = 400
    USE_SUTTON_VERSION = True  # uses bounds and inits from Sutton and Barto's book
    EXAMPLE_ACCELERATION_NOISE = 0.1  # ~10% noise on acceleration
    STATE_FEAT_LEN = 8  # should match len(getStateFeatures())
    GOAL_POS_SUTTON_VERSION = 0.5  # because cos(3*x) is zero close to -0.5, 0.5
    GOAL_POS_WILSON_VERSION = 0.45 # Wilson sets this to 0.45 Agent.m/428, MountainCar/GetReward.m/9
    START_STATE = numpy.array([-0.5, 0])
    def __init__(self, random_state=None, noise=0.0):
        """Initialize a deterministic version by default (no noise)."""
        if MountainCar.USE_SUTTON_VERSION:
            self.state_range = numpy.array([[-1.2, 0.6], [-0.07, 0.07]])
            # The way the constants are set up it seems that the influence of
            # gravity is close to 0 at -0.5 (perhaps this is the bottom of the
            # pit) and 0.5 (this might be the top of the hill).
        else:
            self.state_range = numpy.array([[-1.5, 0.5], [-0.07, 0.07]])
        self.acc = 0.001
        self.gravity = -0.0025
        self.hillFreq = 3.0
        self.noise = noise
        if random_state is None:
            self.random_state = numpy.random.RandomState(None)  # seed with system clock
        else:
            self.random_state = random_state
        # Initialize position, velocity, steps.
        self.reset()

    def reset(self):
        self.time_step = 0
        self.state = MountainCar.START_STATE
        #print "init pos: ", self.state[0], "v ", self.state[1]

    def isAtGoal(self):
        return MountainCar.atGoal(self.time_step, self.state)

    @staticmethod
    def atGoal(steps_taken, state):
        return ((steps_taken >= MountainCar.MAX_STEPS) or
                (MountainCar.USE_SUTTON_VERSION and (state[0] >= MountainCar.GOAL_POS_SUTTON_VERSION)) or
                ((not MountainCar.USE_SUTTON_VERSION) and (state[0] >= MountainCar.GOAL_POS_WILSON_VERSION)))

    def takeAction(self, action):
        # Noise model from RLPy's implementation of Mountain Car.
        noise = self.acc * self.noise * 2 * (self.random_state.rand() - .5)
        # The rest follows either Sutton and Barto's example from RL book or
        # Wilson's MATLAB implementation as closely as possible.
        self.state[1] += noise + self.acc*(action) + self.gravity*numpy.cos(self.hillFreq*self.state[0])
        if not MountainCar.USE_SUTTON_VERSION:
            self.state[1] *= 0.999 # from Wilson: thermodynamic law, for a more real system with friction
        # TODO: Wilson does not enforce speed ranges. Why? (see MountainCar/DoAction.m)
        if MountainCar.USE_SUTTON_VERSION:
            self.state[1] = self.state[1].clip(min=self.state_range[1,0], max=self.state_range[1,1])
        self.state[0] += self.state[1]
        if MountainCar.USE_SUTTON_VERSION:
            self.state[0] = self.state[0].clip(min=self.state_range[0,0], max=self.state_range[0,1])
        if (self.state[0] <= self.state_range[0,0]):
            self.state[0] = self.state_range[0,0]
            self.state[1] = 0.0
        self.time_step += 1
        #print "pos: ", self.state[0], "v ", self.state[1]

    @staticmethod
    def getStateFeatures(state):
        """Extract features from state - dimension 8"""
        l = state[0]
        u = state[1]
        # Dexter's softmax 16-dim policy features:
        return numpy.array([l, u, l * l, l * u, u * u, l * l * u, l * u * u, l * l * u * u])
        # Wilson's policy features:
        #return numpy.array([1, l, l*l, l*l*l, u*l, u*l*l, u*l*l*l, u*u])
        # Rika's 8-dim features (trying to find ones that would give resoanble performance).
        #return numpy.array([1, l, u, l*l, l*u, u*u, l*l*u, l*u*u])

    @staticmethod
    def getSoftmaxProbability(policy_parameters, state_features):
        """Get the probability of action "left" using softmax function"""
        leftPolicy = numpy.dot(policy_parameters[:8], state_features)
        rightPolicy = numpy.dot(policy_parameters[8:], state_features)
        maxPolicy = max(leftPolicy, rightPolicy)
        minPolicy = min(leftPolicy, rightPolicy)
        return math.exp(leftPolicy - maxPolicy - math.log(1 + math.exp(minPolicy - maxPolicy)))

    @staticmethod
    def getSoftmaxAction(policy_parameters, state_features, random_state, deterministic_policy):
        """Get the probability of action "left" using softmax function and
        use it as a threshold to decide whether to take left or right action
        deterministically."""
        prob_left = MountainCar.getSoftmaxProbability(policy_parameters, state_features)
        if deterministic_policy:
            return -1 if (prob_left > 0.5) else 1
        else:
            rnd_num = random_state.rand(1)[0]
            #print "rnd_num ", rnd_num, " prob_left ", prob_left, " leftPolicy ", leftPolicy, " rightPolicy ", rightPolicy
            return -1 if (rnd_num <= prob_left) else 1

    @staticmethod
    def getLogisticProbability(policy_parameters, state_features):
        """Get the probability of action "left" using logistic function"""
        return 1.0/(1.0+math.exp(-(numpy.dot(policy_parameters, state_features))))


    @staticmethod
    def getWilsonProbability(policy_parameters, state_features):
        """Get the probability of action "left" using the very simple heuristic
        that appears to be used in Wilson's code"""
        return 0.7 if numpy.dot(policy_parameters, state_features) < 0 else 0.3

    @staticmethod
    def getWilsonAction(policy_parameters, state_features):
        if numpy.dot(policy_parameters, state_features) < 0:
            return -1
        else:
            return 1

    @staticmethod
    def getAction(policy_parameters, state_features, random_state):
        #return MountainCar.getWilsonAction(policy_parameters, state_features)
        prob_left = MountainCar.getSoftmaxProbability(policy_parameters, state_features)
        rnd_num = random_state.rand(1)[0]
        return -1 if (rnd_num <= prob_left) else 1

    @staticmethod
    def getReward(steps_taken):
        #return steps_taken
        return (1.0*steps_taken-MountainCar.MAX_STEPS)/MountainCar.MAX_STEPS  # prior at 0.0

# Relevant pieces of Wilson's MATLAB code:
# from Agent.m
"""
       function f = GetFeatures(obj,step,maxsteps,action,state,nextstate)
                          f(1) = state(1);
                   c = 1;
%                    if(nextstate(1) <= -1.5 && action < 0)
%                        c=0;
%                    end

                   f(2) = c*state(2);
                   f(3) = c*cos(3*state(1));
                   f(4) = c*action;
                   f(5) = 1;
"""
"""
 function [action,likli,gradient,actionindex,features] = GetBestAction(obj,policy,state,testactionindex,testaction,features)
                   c=10;
                   l = state(1);
                   u = state(2);
                   features = [1,l,l^2,l^3,u*l,u*l^2,u*l^3,u^2]*c;

                   if nargin == 4
                       if policy*features(1:end)' < 0
                           action = -1;
                           actionindex = 1;
                       else
                           action = 1;
                           actionindex = 2;
                       end
                       if actionindex == testactionindex
                           likli = .7;
                       else
                           likli = .3;
                       end
                       action = testactionindex;
                   else
                       if policy*features(1:end)' < 0
                           action = -1;
                           actionindex = 1;
                       else
                           action = 1;
                           actionindex = 2;
                       end
                       likli = .7;
                   end
                   %                    action = tanh(60*(policy*features'));
                   gradient = zeros(1,length(policy));

                   %                    likli  = normpdf(0,0,2);
                   %                     likli = .8;
"""

"""
       function x = GetInitialState(obj)
                   x = [-0.5,0];
"""

"""
       function term = CheckTermination(obj,x)
                   position = x(1);
                   bpright=0.45;
                   if( position >= bpright)
                       term = true;
                   end
"""